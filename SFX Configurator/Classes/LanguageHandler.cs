﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_SFX_Config.Classes
{
    class LanguageHandler
    {
        private List<Language> _languages = new List<Language>();

        internal List<Language> Languages { get => _languages; }
    }
}
