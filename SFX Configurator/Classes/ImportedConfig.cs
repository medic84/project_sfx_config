﻿using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;

namespace Project_SFX_Config
{
    class ImportedConfig
    {
        private List<ImportedConfigItem> _items = new List<ImportedConfigItem>();

        internal List<ImportedConfigItem> Items { get => _items; }

        public ImportedConfig(string filePath)
        {
            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException(filePath);
            }
            this.Import(filePath);
        }

        private void Import(string filePath)
        {
            foreach (Match match in this.GetMatchesFromFile(filePath))
            {
                var regex = new Regex(@"(?s)(;?\x20*\t*\w+\d?)\s*=\s*(?:""(.+?)""|(-))(?=\s*[\r\n]|$)", RegexOptions.None);
                foreach(Match paramMatch in regex.Matches(match.Groups[2].Value))
                {
                    var item = new ImportedConfigItem(match.Groups[1].Value)
                    {
                        Command = paramMatch.Groups[1].Value.Replace(" ", ""),
                        Parameter = (paramMatch.Groups[3].Value == "-") ? paramMatch.Groups[3].Value : paramMatch.Groups[2].Value
                    };
                    this._items.Add(item);
                }
            }
        }

        private MatchCollection GetMatchesFromFile(string filePath)
        {
            var stream = new StreamReader(filePath);
            var regex = new Regex(
                @";!@Install@!UTF-8((?:\:x\d{2,2})?(?:\:Language\:\d{4,5})?)!\s?(.+?)\s?;!@InstallEnd@(?:\:x\d{2})?(?:\:Language\:\d{4,5})?!",
                RegexOptions.Singleline
                );
            return regex.Matches(stream.ReadToEnd());
        }
    }

    class ImportedConfigItem
    {
        private string _section, _command, _parameter;
        private int _index = 4;

        public ImportedConfigItem(string section)
        {
            this.Section = section;
        }

        public string Section { get => _section; set => _section = value; }
        public string Command
        {
            get => _command;
            set {
                _command = value.Trim();
                switch (_command)
                {
                    case "Delete":
                        Index = 3;
                        break;
                    case "Shortcut":
                        Index = 0;
                        break;
                    case "SetEnvironment":
                    case "InstallPath":
                        Index = 1;
                        break;
                    case "RunProgram":
                    case "ExecuteFile":
                    case "ExecuteParameters":
                    case "AutoInstall":
                        Index = 2;
                        break;
                }
            }
        }
        public string Parameter { get => _parameter; set => _parameter = value; }
        public int Index { get => _index; set => _index = value; }
    }
}
