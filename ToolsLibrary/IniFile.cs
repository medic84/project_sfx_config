﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Runtime.InteropServices;

namespace ToolsLibrary
{
    public class IniFile
    {
        public const int MaxSectionSize = 32767; // 32 KB

        private readonly string _mPath;

        #region P/Invoke declares

        [System.Security.SuppressUnmanagedCodeSecurity]
        private static class NativeMethods
        {
            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            public static extern int GetPrivateProfileSectionNames(IntPtr lpszReturnBuffer,
            uint nSize,
            string lpFileName);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            public static extern uint GetPrivateProfileString(string lpAppName,
            string lpKeyName,
            string lpDefault,
            StringBuilder lpReturnedString,
            int nSize,
            string lpFileName);

            /*
                        [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
                        public static extern uint GetPrivateProfileString(string lpAppName,
                        string lpKeyName,
                        string lpDefault,
                        [In, Out] char[] lpReturnedString,
                        int nSize,
                        string lpFileName);
            */

            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            public static extern int GetPrivateProfileString(string lpAppName,
            string lpKeyName,
            string lpDefault,
            IntPtr lpReturnedString,
            uint nSize,
            string lpFileName);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            public static extern int GetPrivateProfileInt(string lpAppName,
            string lpKeyName,
            int lpDefault,
            string lpFileName);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto)]
            public static extern int GetPrivateProfileSection(string lpAppName,
            IntPtr lpReturnedString,
            uint nSize,
            string lpFileName);

            [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
            public static extern bool WritePrivateProfileString(string lpAppName,
            string lpKeyName,
            string lpString,
            string lpFileName);


        }
        #endregion
        public IniFile(string path)
        {

            _mPath = System.IO.Path.GetFullPath(path);
        }

        public string Path
        {
            get
            {
                return _mPath;
            }
        }

        public override string ToString()
        {
            return Path;
        }
        #region Get Value Methods

        public string GetString(string sectionName,
        string keyName,
        string defaultValue)
        {
            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            if (keyName == null)
                throw new ArgumentNullException("keyName");

            var retval = new StringBuilder(MaxSectionSize);

            NativeMethods.GetPrivateProfileString(sectionName,
            keyName,
            defaultValue,
            retval,
            MaxSectionSize,
            _mPath);

            return retval.ToString();
        }

        public int GetInt16(string sectionName,
        string keyName,
        short defaultValue)
        {
            int retval = GetInt32(sectionName, keyName, defaultValue);

            return Convert.ToInt16(retval);
        }

        public int GetInt32(string sectionName,
        string keyName,
        int defaultValue)
        {
            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            if (keyName == null)
                throw new ArgumentNullException("keyName");


            return NativeMethods.GetPrivateProfileInt(sectionName, keyName, defaultValue, _mPath);
        }

        public double GetDouble(string sectionName,
        string keyName,
        double defaultValue)
        {
            var retval = GetString(sectionName, keyName, "");

            return string.IsNullOrEmpty(retval) ? defaultValue : Convert.ToDouble(retval, CultureInfo.InvariantCulture);
        }

        #endregion

        #region GetSectionValues Methods

        public List<KeyValuePair<string, string>> GetSectionValuesAsList(string sectionName)
        {
            string[] keyValuePairs;

            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            //Allocate a buffer for the returned section names.
            IntPtr ptr = Marshal.AllocCoTaskMem(MaxSectionSize);

            try
            {
                //Get the section key/value pairs into the buffer.
                int len = NativeMethods.GetPrivateProfileSection(sectionName,
                ptr,
                MaxSectionSize,
                _mPath);

                keyValuePairs = ConvertNullSeperatedStringToStringArray(ptr, len);
            }
            finally
            {
                //Free the buffer
                Marshal.FreeCoTaskMem(ptr);
            }

            //Parse keyValue pairs and add them to the list.
            var retval = new List<KeyValuePair<string, string>>(keyValuePairs.Length);

            for (var i = 0; i < keyValuePairs.Length; ++i)
            {
                //Parse the "key=value" string into its constituent parts
                var equalSignPos = keyValuePairs[i].IndexOf('=');

                var key = keyValuePairs[i].Substring(0, equalSignPos);

                var value = keyValuePairs[i].Substring(equalSignPos + 1,
                                                          keyValuePairs[i].Length - equalSignPos - 1);

                retval.Add(new KeyValuePair<string, string>(key, value));
            }

            return retval;
        }

        public Dictionary<string, string> GetSectionValues(string sectionName)
        {
            List<KeyValuePair<string, string>> keyValuePairs = GetSectionValuesAsList(sectionName);

            //Convert list into a dictionary.
            var retval = new Dictionary<string, string>(keyValuePairs.Count);

            foreach (var keyValuePair in keyValuePairs)
            {
                //Skip any key we have already seen.
                if (!retval.ContainsKey(keyValuePair.Key))
                {
                    retval.Add(keyValuePair.Key, keyValuePair.Value);
                }
            }

            return retval;
        }

        #endregion

        #region Get Key/Section Names

        public string[] GetKeyNames(string sectionName)
        {
            string[] retval;

            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            //Allocate a buffer for the returned section names.
            IntPtr ptr = Marshal.AllocCoTaskMem(MaxSectionSize);

            try
            {
                //Get the section names into the buffer.
                int len = NativeMethods.GetPrivateProfileString(sectionName,
                                                                null,
                                                                null,
                                                                ptr,
                                                                MaxSectionSize,
                                                                _mPath);

                retval = ConvertNullSeperatedStringToStringArray(ptr, len);
            }
            finally
            {
                //Free the buffer
                Marshal.FreeCoTaskMem(ptr);
            }

            return retval;
        }

        public string[] GetSectionNames()
        {
            string[] retval;

            //Allocate a buffer for the returned section names.
            var ptr = Marshal.AllocCoTaskMem(MaxSectionSize);

            try
            {
                //Get the section names into the buffer.
                var len = NativeMethods.GetPrivateProfileSectionNames(ptr,
                                                                      MaxSectionSize, _mPath);

                retval = ConvertNullSeperatedStringToStringArray(ptr, len);
            }
            finally
            {
                //Free the buffer
                Marshal.FreeCoTaskMem(ptr);
            }

            return retval;
        }

        private static string[] ConvertNullSeperatedStringToStringArray(IntPtr ptr, int valLength)
        {
            string[] retval;

            if (valLength == 0)
            {
                //Return an empty array.
                retval = new string[0];
            }
            else
            {
                string buff = Marshal.PtrToStringAuto(ptr, valLength - 1);
                retval = buff.Split('\0');
            }

            return retval;
        }

        #endregion

        #region Write Methods

        private void WriteValueInternal(string sectionName, string keyName, string value)
        {
            if (!NativeMethods.WritePrivateProfileString(sectionName, keyName, value, _mPath))
            {
                throw new System.ComponentModel.Win32Exception();
            }
        }

        public void WriteValue(string sectionName, string keyName, string value)
        {
            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            if (keyName == null)
                throw new ArgumentNullException("keyName");

            if (value == null)
                throw new ArgumentNullException("value");

            WriteValueInternal(sectionName, keyName, value);
        }

        public void WriteValue(string sectionName, string keyName, short value)
        {
            WriteValue(sectionName, keyName, (int)value);
        }

        public void WriteValue(string sectionName, string keyName, int value)
        {
            WriteValue(sectionName, keyName, value.ToString(CultureInfo.InvariantCulture));
        }

        public void WriteValue(string sectionName, string keyName, float value)
        {
            WriteValue(sectionName, keyName, value.ToString(CultureInfo.InvariantCulture));
        }

        public void WriteValue(string sectionName, string keyName, double value)
        {
            WriteValue(sectionName, keyName, value.ToString(CultureInfo.InvariantCulture));
        }

        #endregion

        #region Delete Methods

        public void DeleteKey(string sectionName, string keyName)
        {
            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            if (keyName == null)
                throw new ArgumentNullException("keyName");

            WriteValueInternal(sectionName, keyName, null);
        }

        public void DeleteSection(string sectionName)
        {
            if (sectionName == null)
                throw new ArgumentNullException("sectionName");

            WriteValueInternal(sectionName, null, null);
        }

        #endregion
    }
}
