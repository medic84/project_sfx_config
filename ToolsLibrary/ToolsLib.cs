﻿using System.Text;
using System.Security.Cryptography;
using System.IO;

namespace ToolsLibrary
{
    public class Tools
    {
        public static string GetMD5HashFromFile(string fileName)
        {
            try
            {
                var file = new StreamReader(fileName);

                MD5 md5 = new MD5CryptoServiceProvider();

                var retVal = md5.ComputeHash(file.BaseStream);
                file.Dispose();

                var sb = new StringBuilder();
                for (var i = 0; i < retVal.Length; i++)
                {
                    sb.Append(retVal[i].ToString("x2"));
                }
                return sb.ToString().ToUpper();
            }
            catch
            { return ""; }

        }

        public static int Execute(string adress, string par, bool waitforexit = false)
        {
            var p = new System.Diagnostics.Process
            { StartInfo = { UseShellExecute = false, FileName = adress, Arguments = par } };
            p.Start();
            if (waitforexit)
            {
                p.WaitForExit();
                return p.ExitCode;
            }
            return 0;
        }

        public static int Execute(string adress, bool shellexecute = true)
        {
            var p = new System.Diagnostics.Process { StartInfo = { UseShellExecute = shellexecute, FileName = adress } };
            p.Start();
            return 0;
        }

        public static string ReplaceStrings(string str, char[] aOldChars, char[] aNewChars, bool truing = true)
        {
            var retStr = str;
            if (retStr == null) return null;
            if (aOldChars.Length != aNewChars.Length) return str;
            if (truing)
            {
                for (var i = 0; i < aOldChars.Length; i++)
                    retStr = retStr.Replace(aOldChars[i], aNewChars[i]);
            }
            return retStr;
        }

        public static string ReplaceStrings(string str, string[] aOldChars, string[] aNewChars, bool truing = true)
        {
            var retStr = str;
            if (retStr == null) return null;
            if (aOldChars.Length != aNewChars.Length) return str;
            if (truing)
            {
                for (var i = 0; i < aOldChars.Length; i++)
                    retStr = retStr.Replace(aOldChars[i], aNewChars[i]);
            }
            return retStr;
        }
    }
}

